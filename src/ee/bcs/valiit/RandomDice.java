package ee.bcs.valiit;

import java.util.Random;

public class RandomDice {
    int number;

    public int numberChange(){
        Random random=new Random();
        number=random.nextInt(6)+1;
        return number;
    }

    public int getNumber() {
        return number;
    }
}
