package ee.bcs.valiit;


public class DiceVisual {
    public static String patterns(int diceNumber){
        switch (diceNumber) {
            case 1:
                String newDice = diceVisual().substring(0, 24) + '*' + diceVisual().substring(25);
                return newDice;
            case 2:
                String newDice2 = diceVisual().substring(0, 22) + '*' + diceVisual().substring(23,26) + '*' + diceVisual().substring(27);
                return newDice2;
            case 3:
                String newDice3 = diceVisual().substring(0, 11) + '*' + diceVisual().substring(12,24) + '*' + diceVisual().substring(25,37)+'*'+ diceVisual().substring(38);
                return newDice3;
            case 4:
                String newDice4 = diceVisual().substring(0, 12) + '*' + diceVisual().substring(13,16) + '*' + diceVisual().substring(17,32)+'*'+ diceVisual().substring(33,36)+'*'+diceVisual().substring(37);
                return newDice4;
            case 5:
                String newDice5 = diceVisual().substring(0, 12) + '*' + diceVisual().substring(13,16) + '*'+diceVisual().substring(17,24)+'*' + diceVisual().substring(25,32)+'*'+ diceVisual().substring(33,36)+'*'+diceVisual().substring(37);
                return newDice5;
            case 6:
                String newDice6 = diceVisual().substring(0, 12) + '*' + diceVisual().substring(13,14) + '*'+diceVisual().substring(15,16)+'*'
                        + diceVisual().substring(17,22)+'*'+ diceVisual().substring(23,24)+'*'+diceVisual().substring(25,26)+'*'+diceVisual().substring(27,31)
                        +diceVisual().substring(32,33)+'*'+ diceVisual().substring(34,35)+'*'+diceVisual().substring(36,37)+'*'+diceVisual().substring(37);
                return newDice6;
        }
        return "Nothing";
    }

    public static String diceVisual(){
        StringBuilder diseCube=new StringBuilder();
        diseCube.append("+");
            diseCube.append(horDash("-",7));
            diseCube.append("+");
            for (int j=0;j<3;j++){
                diseCube.append("\n");
                diseCube.append(vertDashes("|",1));
                diseCube.append(empthyValue(" ",7));
                diseCube.append(vertDashes("|",1));
                if (j==2){
                    diseCube.append("\n");
                }
            }
            diseCube.append("+");
            diseCube.append(horDash("-",7));
            diseCube.append("+");
            return diseCube.toString();
    }

    public static String horDash(String str,int times){
        StringBuilder horDashes=new StringBuilder(str.length()*times);
        for(int i=0;i<times;i++){
            horDashes.append(str);
        }
        return horDashes.toString();
    }

    public static String vertDashes(String str,int times){
        StringBuilder vertDash=new StringBuilder(str.length()*times);
        for(int i=0;i<times;i++){
            vertDash.append(str);
        }
        return vertDash.toString();
    }

    public static String empthyValue(String str,int times){
        StringBuilder empthy=new StringBuilder(str.length()*times);
        for(int i=0;i<times;i++){
            empthy.append(str);
        }
        return empthy.toString();
    }
}
